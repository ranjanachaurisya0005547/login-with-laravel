<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Show User Information..............</title>

	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/file_upload.css')}}"/>
	
	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/font-awesome/css/all.css')}}"/>

</head>
<body>
<div class="container-fluid">
	<div class="row">
		<table class="table table-striped">
			<tr class="bg-dark text-white">
				<th>User Id</th>
				<th>Name</th>
				<th>Contact</th>
				<th>Profile</th>
				<th>Edit</th>
				<th>Delete</th>
			</tr>

       @foreach($data as $key)
			<tr>
				<td>{{$key->user_id}}</td>
				<td>{{$key->name}}</td>
				<td>{{$key->mobile}}</td>
				<td>
					<a href="{{asset('userProfile')}}/{{$key->profile}}" target="_blank">
						<img height="70px" width="100px" src="{{asset('userProfile')}}/{{$key->profile}}" alt="image not found"/>
					</a>
				</td>
				<td>
					<a href=""><i class="fa fa-edit icon-color"></i></a>
				</td>
				<td>
					<a onclick="return confirm('Really,Do You want to delete ?');" href="delete/{{$key->user_id}}"><i class="fa fa-trash icon-color"></i></a>
				</td>
			</tr>

	  @endforeach		
		</table>
	</div>
</div>
</body>
</html>