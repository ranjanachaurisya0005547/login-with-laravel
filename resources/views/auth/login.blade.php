@include('layout.header')

<!------------------------BODY PART---------------------------------->

<div class="container-fluid login-form-area">

      <div class="mask align-items-center h-100 mt-5 mb-5 login-area">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-md-8">

          <!--------------------Registration Success Message ---------------------->
            @if(session('success'))
                <div class="alert alert-success mt-2 mb-0">{{session('success')}}</div>
            @elseif(session('failed'))
                <div class="alert alert-danger mt-2 mb-0">{{session('failed')}}</div>
            @endif    

              <form class="bg-white rounded shadow-5-strong p-5" method="POST" action="{{route('transfer')}}">

               @csrf

                <!-- Email input -->
                <div class="form-outline mb-4">
                  <label class="form-label" for="email">Email address</label>
                  <input type="email" name="email" id="email" class="form-control" />
                </div>

                <!-- Password input -->
                <div class="form-outline mb-4">
                  <label class="form-label" for="password">Password</label>
                  <input type="password" name="password" id="password" class="form-control" />
                </div>

                <!-- 2 column grid layout for inline styling -->
                <div class="row mb-4">
                  <div class="col d-flex justify-content-center">
                    <!-- Checkbox -->
                    <div class="form-check">
                      <input class="form-check-input" type="checkbox" value="" id="form1Example3" checked />
                      <label class="form-check-label" for="form1Example3">
                        Remember me
                      </label>
                    </div>
                  </div>

                  <div class="col text-center">
                    <!-- Simple link -->
                    <a href="#">Forgot password?</a>
                  </div>
                </div>

                <!-- Submit button -->
                <input type="submit" class="btn btn-primary btn-block" value="Sign In">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

<!-----------------------END BODY PART------------------------------->
@include('layout.footer')