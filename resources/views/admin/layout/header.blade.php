<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Admin Dashboard...............</title>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/bootstrap/css/bootstrap.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/dashboard.css')}}"/>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/css/user_info.css')}}"/>

	<script src="{{asset('assets/js/jquery.js')}}"></script>
	<script src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
	<link type="text/css" rel="stylesheet" href="{{asset('assets/font-awesome/css/all.css')}}"/>
</head>
<body>
<div class="container-fluid outer">
	<div class="row top-header">
		<div class="col-sm-3 header-title">Admin Dashboard</div>
		<div class="col-sm-6 header-center"></div>
		<div class="col-sm-3 header-right">
			<i class="fa fa-bell"></i>&nbsp;&nbsp;
			<i class="fa fa-user"></i>
		</div>
	</div>

	<div class="row body-part">
		<div class="col-sm-2 left-menu-area">
			<div class="row link"><a href="{{url('/dashboard')}}">Home</a></div>
			<div class="row link"><a href="">Gallery</a></div>
			<div class="row link"><a href="">Feedback</a></div>
			<div class="row link"><a href="{{url('/user_Info')}}">User Information</a></div>
			<div class="row link"><a href="{{url('logout')}}">Logout</a></div>
		</div>
		<div class="col-sm-10 right-content-area">