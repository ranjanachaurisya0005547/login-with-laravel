<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class userapiseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
               "name"=>"gaurav",
               'mobile'=>6307972183,
               "email"=>"ranjana@gmail.com",
               "password"=>\Hash::make('ranjana@123'),
        ]);

    }
}
