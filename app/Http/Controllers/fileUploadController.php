<?php 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\fileUpload;
use Validator;

class fileUploadController extends Controller{


   //show user form
   public function show(){
      return view('file-upload.file-upload');
   }



   //store user data in database
   public function stores(Request $request){
			       
	$rule=[
		'name'=>'required|regex:/^[A-Za-z ]{1,}$/',
		'mobile'=>'required|min:10|max:12|regex:/^[6-9]{1}[0-9]{9}$/',
              'profile' => 'required|file|mimes:jpg,jpeg,docx,txt,pdf,zip',
       ];

       $validate=Validator::make($request->all(),$rule);

       if($validate->fails()){
              return redirect()->to(route('form'))->withErrors($validate)->withInput();
       }else{
              try{

                    $data=$request->input();
		      $file_obj=$request->file('profile');
	             // $new_file=$file_obj->extension();
	             $fileName=$file_obj->getClientOriginalName();


                    $file_obj->move(public_path('userProfile'),$fileName);

                    $user_data=fileUpload::create([
                          'name'=>$data['name'],
                          'mobile'=>$data['mobile'],
                          'profile'=>$fileName
                    ]);

                    $user_data->save();
                    return redirect()->to(route('form'))->with("success","Registration Done Successfully !");
		   
		}catch(\Exception $ex){
                    return redirect()->to(route('form'))->with('failed',$ex->getMessage());
		} 

       }
   }

   
   //show user data
   public function showData(){
   	$data=fileUpload::all();

   	return view('file-upload.show-profile',['data'=>$data]);
   }


   //delete user
   public function delete($id){
   	$user=fileUpload::where('user_id',$id);
   	$user->delete();

   	return back()->with("success","Deleted Successfully !");
   }


   //edit user
   public function editForm(Request $request,$id){
   	$data=fileUpload::where('user_id',$id)->get();

   	return view('file-upload.edit',['data'=>$data]);
   }


   //update user
   public function update(){
   	echo "weldone";
   }
  
}