<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Registration;
use Validator;

class RegistrationController extends Controller
{

    //Show SignUp Form
    public function index(){
        return view("auth.signup");
    }


    //Store User Data
    public function store(Request $request){
        $data=$request->input();

        $rule=[
           'name'=>'required|regex:/^[A-Za-z ]+$/|max:70',
           'email'=>'required',
           'mobile'=>'required|min:10|max:12',
           'password'=>'required|min:8|max:32|Confirmed|regex:/^[A-Za-z]{1,}[0-9]{1,}[\@#$%\*()\{\}\[\]_\.]{1,}/',
           'status'=>'required'

        ];

        $validate=Validator::make($request->all(),$rule);

        if($validate->fails()){
            return redirect('/signup')->withErrors($validate)->withInput();
        }else{

            try{
                  $insertUser=new Registration();
                  if($insertUser->insertData($data)){
                       return redirect('/login')->with("success","Registration successful,Login Here...");
                  }else{
                       return back()->with('failed',"Registration Failed !");
                  }
             }catch(\Exception $ex){
                return back()->with('failed',$ex->getMessage());
            }
              
              
        }
        
    }



    


}

