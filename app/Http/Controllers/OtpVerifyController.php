<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\OtpVerify;

class OtpVerifyController extends Controller
{
    public function index(){
        return view('otp-generation.otp');
    }
    public function generateOTP(){
        $otp = mt_rand(1000,9999);
        return $otp;
    }

    public function submitForm(){
        $name = request('name');
        $mobile = request('mobile');
        $email = request('email');
        $authKey =  env('AUTH_KEY',"");
        if($mobile==''){
            return json_encode(array('statusCode'=>400,'msg'=>"Mobile number not valid".$mobile));
        }else{
            $otp = $this->generateOTP();
            $message = 'you otp is '.$otp;
            		
           session(['name'=> $name]);
           session(['mobile'=> $mobile]);
           session(['email'=> $email]);
           session(['otp' => $otp]);
            return json_encode(array('statusCode'=>200,'msg'=>'otp sent successfully'.$otp));
        }
        
    }
    public function submitOtp(){
        $otp = trim(request('otp'));
        dd(request('otp'));
        if($otp==''){
            return json_encode(array('statusCode'=>400,'msg'=>"otp not valid"));
        }
        else{
            $user = new OtpVerify;
            if($otp == session('otp')){
            $name = session('name');
            $mobile = session('mobile');
            $email = session('email');
            $user->save();
            session()->flush();
            json_code(array('statusCode'=>200,'msg'=>'sucess'));

            }
            else{
                return json_encode(array('statusCode'=>400,'msg'=>"otp not valid"));
            }
        }
    }
    public function show(){
        return view('otp-generation.home');
    }

}
